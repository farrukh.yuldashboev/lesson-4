// Assignment 4


function charReverseHandler (str) {
    var reversedString = [];
    for (char of str) {
        if (char == char.toUpperCase()){
            reversedString.push(char.toLowerCase());
            // var charLower = char.replace(char, char.toLowerCase());
            // reversedString.push(charLower)
        } else if (char == char.toLowerCase()) {
            reversedString.push(char.toUpperCase());
        }else if (char == " ") {
            reversedString.push(char);
        }
    }
    console.log(reversedString.join(""));
};

function createListHandler () { //prompt method
    var todoArr = []
    var itemCount = 1
    var runApp = true
    while (runApp) {
        var input = prompt("Type to add an item: | commands: exit , ls , reset")
        if (input == "exit" || input == "exit()") {
            runApp = false
        } else if (input == "ls" || input == "list items"){
            alert(todoArr.join(" "))
        } else if (input == "reset") {
            todoArr = []
            itemCount = 1
        }else {
            todoArr.push(`${itemCount}. ` + input)
            itemCount++
        }

        //alert(todoArr + " is added to TODoApp");
    
    }
    
};

function toDoApp () { //dom method
    let userInput = document.querySelector(".user_input")
    let submitButton = document.querySelector(".submit-btn")
    let listItems = document.querySelector(".listItems")

    submitButton.addEventListener("click", function() {
        var paragraph = document.createElement("li")
        paragraph.innerText = userInput.value
        listItems.appendChild(paragraph)
        userInput.value = null
    })
}


function findDublicatedHandler (array) {
    var duplicatesArr = []
    for (var i = 0; i < array.length; i++) {
        for (var j = i+1; j < array.length; j++) {
            if (array[i] === array[j]) {
                duplicatesArr.push(array[j])
            }
        }

    }
    console.log(duplicatesArr);
}

function unionHandler (arr1, arr2) {
    var unionArr = []
    console.log(unionArr.concat(arr1, arr2));
}

function unionHandler () {
    // your code here
}

function unusedRemover (array) {
    var removedArr = (array.filter(Boolean));
    console.log(removedArr);
}

function reverseStringHandler (str) {
    console.log(str.split("").sort((a, b) => a.localeCompare(b, undefined, {sensitivity: 'base'})))
}



/* Test Cases */
// charReverseHandler('The Quick Brown Fox')
// createListHandler()
// toDoApp()  //please check in live server
// findDublicatedHandler([2,14,5,14,1,6])
//unionHandler([1,2,3], [2,3,4])
//unusedRemover(["items", ,"removed", "successfully", null, 0, "", false, undefined, NaN, "!"])
reverseStringHandler("WebmASter")